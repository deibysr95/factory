public class ReglaSaldoInsuficiente implements reglas {
    @Override
    public String execute(Transaccion transaccion){
        if(transaccion.montoMovimiento > transaccion.cuenta.monto){
            return "Saldo insuficiente ";
        }
        return "";
    }
}
