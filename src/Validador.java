import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Validador {
private final  List<Regla > rules ;

public Validator (List<>)
    List<String> validarTransaccion(Transaccion transaction) {
        List<String> reglasVioladas = new ArrayList<>();

        ReglasCuentaActiva reglaCuenta = new ReglasCuentaActiva();

        if (!reglaCuenta.execute(transaction).isEmpty()) {
            reglasVioladas.add(reglaCuenta.execute(transaction));
        }

        ReglaSaldoInsuficiente reglaSaldoInsuficiente = new ReglaSaldoInsuficiente();

        if (!reglaSaldoInsuficiente.execute(transaction).isEmpty()) {
            reglasVioladas.add(reglaSaldoInsuficiente.execute(transaction));
        }

        ReglaMontoValido reglaMontoValido = new ReglaMontoValido();

        if (!reglaMontoValido.execute(transaction).isEmpty()) {
            reglasVioladas.add(reglaMontoValido.execute(transaction));
        }


        if (!transaction.cuenta.activa) {
            reglasVioladas.add("la cuenta no esta activa ");
        }

        if (transaction.montoMovimiento > transaction.cuenta.monto) {
            reglasVioladas.add("el monto es menor al monto de la transaccion  ");
        }

        if (transaction.fecha.getTime() < (new Date().getTime() - 3000)) ;
        return reglasVioladas;
    }


}
