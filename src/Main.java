import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {


        System.out.println("Compilado");
        Cuenta cuentaSebas = new Cuenta();
        cuentaSebas.nombreUsuario = "Deibys";
        cuentaSebas.activa = false;
        cuentaSebas.tipo = "Ahorros";
        cuentaSebas.monto = 100000.0;

        Transaccion pagoSemestre = new Transaccion();
        pagoSemestre.fecha = new Date();
        pagoSemestre.montoMovimiento = 300000.0;
        pagoSemestre.historico = new ArrayList<>();
        pagoSemestre.cuenta = cuentaSebas;

        Validador validator = new Validador();
        List<String> reglasVioladas = validator.validarTransaccion(pagoSemestre);

        System.out.println(cuentaSebas.nombreUsuario + "monto" + cuentaSebas.monto);
        System.out.println(pagoSemestre.fecha + "montoTransaccion" + pagoSemestre.montoMovimiento);
        System.out.println(reglasVioladas.stream().collect(Collectors.joining(",")));


    }
}
